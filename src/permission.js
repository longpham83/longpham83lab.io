import router, { resetRouter } from './router'
import store from './store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import getPageTitle from '@/utils/get-page-title'
import constants from '@/utils/constants'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect', '/muarau','/muarau_success'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = localStorage.getItem(constants.ACCESS_TOKEN)
  console.log('Token:' + hasToken)
  if (hasToken !== undefined && hasToken !== null && hasToken === 'admin') {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      console.log('case 1')
      next({ path: '/' })
      NProgress.done() // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    } else {
      console.log('case 2')
      const accessRoutes = await store.dispatch('permission/generateRoutes', [])
      resetRouter()
      router.addRoutes(accessRoutes)
      next()
    }
  } else {
    /* has no token*/

    const foundIdx = whiteList.findIndex(item => to.path.indexOf(item) >= 0)
    console.log('Found: ' + foundIdx)

    if (foundIdx >= 0) {
      // in the free login whitelist, go directly
      console.log('case 3')
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      console.log('case 4')
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
