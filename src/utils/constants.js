module.exports = {
  ACCESS_TOKEN: 'access_token',
  BASE_API_ENDPOINT:'base_api_endpoint',

  DB_LOCATION_TABLE:'locations',
  DB_PRODUCT_TABLE:'products',
  DB_ORDERS_TABLE:'orders',
  DB_ORDER_DETAIL_TABLE:'order_details',
  DB_ORDER_PRODUCTS_TABLE:'order_products',

  //SH
  // DB_LOCATION_TABLE:'locations_1',
  // DB_PRODUCT_TABLE:'products_1',
  // DB_ORDERS_TABLE:'orders_1',
  // DB_ORDER_DETAIL_TABLE:'order_details_1',
}
