export function stringToASCII(str) {
    try {
      return str.replace(/[àáảãạâầấẩẫậăằắẳẵặ]/g, 'a')
        .replace(/[èéẻẽẹêềếểễệ]/g, 'e')
        .replace(/[đ]/g, 'd')
        .replace(/[ìíỉĩị]/g, 'i')
        .replace(/[òóỏõọôồốổỗộơờớởỡợ]/g, 'o')
        .replace(/[ùúủũụưừứửữự]/g, 'u')
        .replace(/[ỳýỷỹỵ]/g, 'y')
    } catch {
      return ''
    }
}

export function searchASCII(input,search) {
    var strOriginalToASCII = stringToASCII(input.toLowerCase())
    var strSearchToASCII = stringToASCII(search.toLowerCase())
    var result = strOriginalToASCII.includes(strSearchToASCII)
    return result
}
