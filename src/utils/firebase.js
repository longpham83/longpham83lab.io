import firebase from "firebase/app"
import "firebase/firestore"

const settings = {timestampsInSnapshots: true};
const firebaseConfig = {
    apiKey: "AIzaSyCaKye-UdSEtmBQQiPG9ciyCGAyO5WRDYg",
    authDomain: "order-mgmt-d360c.firebaseapp.com",
    projectId: "order-mgmt-d360c",
    storageBucket: "order-mgmt-d360c.appspot.com",
    messagingSenderId: "762604926680",
    appId: "1:762604926680:web:8b9262847b88665a8689ad"
  };


firebase.initializeApp(firebaseConfig)

firebase.firestore().settings(settings)


export default firebase.firestore()

  