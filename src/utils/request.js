import axios from 'axios'
import { Message } from 'element-ui'
import API from '@/api/endpoints'
import constants from '@/utils/constants'
import endpoints from '@/api/endpoints'
// create an axios instance
const service = axios.create({
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 30000, // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    // if (store.getters.token) {
    //   // let each request carry token
    //   // ['X-Token'] is a custom headers key
    //   // please modify it according to the actual situation
    //   config.headers['X-Token'] = getToken()
    // }
    
    const base_endpoint = localStorage.getItem(constants.BASE_API_ENDPOINT)
    if (base_endpoint && base_endpoint !== null && base_endpoint !== undefined) {
      config.url = base_endpoint + config.url
    }else {
      config.url = window.location.origin + config.url
    }

    console.log("Before Request:" + config.url)

    const access_token = localStorage.getItem(constants.ACCESS_TOKEN)
    if (access_token) {
      config.headers['X-Auth-Token'] =  access_token
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    
    if (response.status === 200 || response.status == 201) {
      return res
    } else if(response.status === 401 || response.status === 403){
      window.location.href = '/'
    } 
    else {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
    }
  },
  error => {
    const apiHttpCode = error.response && error.response.status;
    if (apiHttpCode && +apiHttpCode === 400) {
      const msg =  error.response.data.msg[0]
      Message({
        message: msg.message,
        type: 'error',
        duration: 5 * 1000
      })
    }
    
    return Promise.reject(error)
  }
)

export default service
