
module.exports = {
  // baseUrl: 'http://10.8.4.55:8080/',
  baseUrl: 'http://127.0.0.1:8080',
  login: '/auth/login',
  logout: 'auth/logout',
  search_app: '/api/app/search',
  app: '/api/app/save',
  config:'/config.json',
  //version
  create_version: '/api/appVersion/save',
  search_version: '/api/appVersion/search/{appId}',
  qrcode:'/api/appVersion/{appId}/qrcode/520/520',
  update_setting:'/api/setting/update',
  update_collection_setting:'/api/setting/update_collection',
  search_setting: '/api/setting/search',
  search_error_log:'/api/errorLog/search'
}
