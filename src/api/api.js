import request from '@/utils/request'
export function apiGet(url,query) {
    return request({
      url: url,
      method: 'get',
      params: query
    })
}

export function apiPost(url,data) {
    return request({
      url: url,
      method: 'post',
      data
    })
}


export function apiPut(url,data) {
    return request({
      url: url,
      method: 'put',
      data
    })
}

export function apiDelete(url) {
    return request({
      url: url,
      method: 'delete'
    })
}