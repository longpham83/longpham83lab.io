import request from '@/utils/request'
import API from '@/api/endpoints'
export function login(data) {
  return request({
    url: API.login,
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: API.logout,
    method: 'post'
  })
}
