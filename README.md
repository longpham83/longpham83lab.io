h1. Guideline

h3. *1. Installation & Configure*

*1.1 Installation*

* Download Ruby on this link https://rubyinstaller.org/ and follow the guide line on https://www.ruby-lang.org/vi/documentation/installation/

* Install jekyll bundle on this link: https://jekyllrb.com/

*1.2 Configure*

* Configure file: '_config.yml'

* See this link for more detail: https://jekyllrb.com/docs/home/

h3.  *2. Using*

*2.1 Start server, build bundle, update _data and run website*

* To start the Jekyll server run:
<pre>
  $ 'bundle exec jekyll serve'
</pre>
  - After that, using your web browser, then access the address 'http://localhost:4000'

* Config localize at: default.haml, in code lines below:
<pre>
  {% assign path = page.url | replace: 'index.haml','' | replace: '/zh','' | replace: '/ja','' %}
  %ul.list-unstyled
    %li
      %a{:href =>"{{path}}"} English
    %li
      %a{:href => "/zh{{path}}"}中文
    %li
      %a{:href => "/ja{{path}}"}日本語
</pre>
* To set langues that a page support, using "langs" variable.
- Ex1:The page support Chinese then define as below:
<pre>
  ---
  title: News
  langs: zh
  ---
</pre>
- Ex2:The page support Chinese, Japanse then define as below:
<pre>
  ---
  title: News
  langs: zh,ja
  ---
</pre>

* Build multiple langues, run file '_build_lang.sh'

* Build .HAML to .HTML: run file '_make_html.sh'

* Update 'locations.csv', 'redirects.csv', 'messages.json' run file '_update.sh'

*2.2 Add new post in old category*

* Add a new post in old category, assume old category is 'news':

        - Go to _posts/news

        - Create new file with file name follow format : year-month-day-title.html

       with 'year', 'month', 'day', 'title' are the year, month, day, title of your post

*2.3 Add new post in new category*

* Add a new post in new category:

  - Go to _posts folder

  - Create a new folder that has name 'FOLDERNAME'

  - In 'FOLDERNAME', create a file with name follow format: year-month-day-title.html

  with 'year', 'month', 'day', 'title' are the year, month, day, title of your post

* Configure new category, go to '_config.yml', add new category by the statements: 
<pre>
  -
    scope:
      path: "_posts/FOLDERNAME"
    values:
      category: CATEGORYNAME
      layout: "job"
      permalink: /CATEGORYNAME/:title/
</pre>


* Configure 'zh' langue for 'CATEGORYNAME' category, add below statements:
<pre>
  -
    scope:
      path: "_posts/zh/FOLDERNAME"
    values:
      category: CATEGORYNAME
      layout: "CATEGORYLAYOUT"
      lang: zh
      permalink: /zh/CATEGORYNAME/:title/
</pre>
  +*Note:*+
> - 'CATEGORYLAYOUT' template is used to format layout for category's posts.
> 
> - 'lang' is langue of your post (if you do not add this property, default it is 'en')
> 
> - 'permalink' is path where your post file will be generated. See detail at link https://jekyllrb.com/docs/permalinks/
> 
> - See link: https://jekyllrb.com/docs/configuration/ for more detail.

* Format template for page 'CATEGORYNAME' list

  - At where that you need add 'CATEGORYNAME' list, add below code:
<pre>
  {% for post in site.categories.CATEGORYNAME %}
  {% if post.lang == page.lang %}
    %li
      .col-lg-4
        {{ post.city }}, {{ post.country }}
      .col-lg-8
        %a{href:"{{ post.url }}"} 
          {{ post.title }}
  {% endif %}
  {% endfor %}
</pre>

h3. *3. Features*

* Works with Jekyll >= 3.3.0

* Supports English - en (default langue),  Chinese - zh and Japanese - ja

* Works with the --watch flag turned on and will rebuild all site automatically

* Supports generate template files




---------------------------------------------------

h1. Issues Note

h3. *1. Title of news list page is wrong*
* See the link: https://www.fcscs.com/news/, https://www.fcscs.com/zh/news/*
*  Fix: Go to news/index.haml, change from "h1=News" to "h1=m.news"

h3. *2. Link is wrong when click on the "en" link to switche langue*
* See link for more details: https://www.fcscs.com/zh/news/2017/06/19/%E6%96%B0%E4%B8%80%E4%BB%A3FCS-Connect%E6%8F%90%E9%AB%98%E9%85%92%E5%BA%97%E5%AE%BE%E5%AE%A2%E8%AF%B7%E6%B1%82%E6%95%88%E7%8E%87%E7%9A%84%E6%A0%87%E5%87%86/
*  Fix:
- Change file name of post file on Chinesse, Japan version as same as with English version.
- Go to default.haml, at the code config localize, change to:
<pre>
{% assign path = page.url | replace: 'index.haml','' | replace: '/zh','' | replace: '/ja','' %}

%ul.list-unstyled
  %li
    %a{:href =>"{{path}}"} English
  %li
    %a{:href => "/zh{{path}}"}中文
  %li
    %a{:href => "/ja{{path}}"}日本語
</pre>

h3. *3. When hover on icons of social network at the footer, some icons is view be wrong*
* Fix:
- Go to default.haml, add class 'social-network-block' for this block.
- Go to screen.css.scss, add code lines below:
<pre>
.social-network-block {
  a:hover {
    text-decoration: none;
    opacity: 0.8;
    transition: opacity 0.3s ease;
  }
}
</pre>




